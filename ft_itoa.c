/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kjaafary <kjaafary@student.42istanbul.com  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/08/07 11:38:12 by kjaafary          #+#    #+#             */
/*   Updated: 2022/08/21 10:32:50 by kjaafary         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	reslen(int n)
{
	int	i;

	i = 0;
	while (n)
	{
		n /= 10;
		i++;
	}
	return (i);
}

static char	*ft_strrev(char *str)
{
	char	tem;
	int	start;
	int	end;

	start = 0;
	end = ft_strlen(str) - 1;
	while (start < end)
	{
		tem = str[start];
		str[start] = str[end];
		str[end] = tem;
		start++;
		end--;
	}
	return (str);
}

static void	negcheck(int *n, unsigned int *number, size_t *nlen)
{
	if (*n <= 0)
	{
		*number = -(*n);
		(*nlen)++;
	}
}

char	*ft_itoa(int n)
{
	char	*res;
	size_t	i;
	size_t	nlen;
	unsigned int	num;

	i = 0;
	nlen = 0;
	num = n;
	negcheck(&n, &num, &nlen);
	nlen += reslen(n);
	res = malloc(sizeof(char) * (nlen + 1));
	if (!res)
		return (NULL);
	if (n == 0)
		res[i++] = '0';
	while (num)
	{
		res[i++] = (num % 10) + '0';
		num /= 10;
	}
	if (n < 0)
		res[i++] = '-';
	res[i] = 0;
	return (ft_strrev(res));
}
