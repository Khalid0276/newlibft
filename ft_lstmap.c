/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kjaafary <kjaafary@student.42istanbul.com  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/08/13 20:19:52 by kjaafary          #+#    #+#             */
/*   Updated: 2022/08/21 15:07:09 by kjaafary         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, void *(*f)(void *), void (*del)(void *))
{
	t_list	*new_lst;
	t_list	*new_position;
	t_list	*old_position;

	if (!lst || !(new_lst = ft_lstnew((*f)(lst->content))))
		return (NULL);
	new_position = new_lst;
	old_position = lst->next;
	while (old_position)
	{
		if (!(new_position->next = ft_lstnew((*f)(old_position->content))))
		{
			ft_lstclear(&new_lst, del);
			return (NULL);
		}
		new_position = new_position->next;
		old_position = old_position->next;
	}
	return (new_lst);
}
