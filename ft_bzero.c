/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bzero.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kjaafary <kjaafary@student.42istanbul.com  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/26 12:36:18 by kjaafary          #+#    #+#             */
/*   Updated: 2022/08/21 16:32:38 by kjaafary         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_bzero(void *s, size_t n)
{
	ft_memset(s, '\0', n);
}
// #include "stdio.h"
// int main()
// {
// 	int i[4] = {222, 256, 300 ,3000};
// 	ft_bzero(i, 14);

// 	printf("%d \n", i[0]);
// 	printf("%d \n", i[1]);
// 	printf("%d \n", i[2]);
// 	printf("%d", i[3]);
// }