/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kjaafary <kjaafary@student.42istanbul.com  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/26 12:40:51 by kjaafary          #+#    #+#             */
/*   Updated: 2022/08/21 16:24:24 by kjaafary         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dst, void *src, size_t n)
{
	char	*psrc;
	char	*pdst;
	char	*temp;
	size_t	i;
	size_t	j;

	psrc = (char *) src;
	pdst = (char *) dst;
	temp = (char *) malloc(n * sizeof(char));
	i = 0;
	j = 0;
	while (i < n)
	{
		temp[i] = psrc[i];
		i++;
	}
	while (j < n)
	{
		pdst[j] = temp[j];
		j++;
	}
	free(temp);
	return (dst);
}
