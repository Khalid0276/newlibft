/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kjaafary <kjaafary@student.42istanbul.com  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/06/26 12:44:00 by kjaafary          #+#    #+#             */
/*   Updated: 2022/09/14 16:26:52 by kjaafary         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strnstr(const char *big, const char *small, size_t len)
{
	size_t	i;
	size_t	smallen;

	if (*small == 0)
		return ((char *) big);
	smallen = ft_strlen(small);
	i = 0;
	while (i < len && big[i])
	{
		if (big[i] == *small && len - i >= smallen
			&& ft_strncmp(&big[i], small, smallen) == 0)
			return ((char *) &big[i]);
		i++;
	}
	return (NULL);
}
