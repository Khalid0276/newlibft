/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_substr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: kjaafary <kjaafary@student.42istanbul.com  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/08/13 21:03:07 by kjaafary          #+#    #+#             */
/*   Updated: 2022/08/14 11:44:11 by kjaafary         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_substr(char const *s, unsigned int start, size_t len)
{
	char	*newstr;
	size_t	index;

	if (!s)
		return (NULL);
	if (len <= 0 || (start + 1 > ft_strlen(s)))
	{
		newstr = (char *) malloc(1);
		*newstr = 0;
		return (newstr);
	}
	newstr = (char *) malloc((len + 1) * sizeof(char));
	if (!newstr)
		return (NULL);
	index = 0;
	if (start < ft_strlen(s))
	{
		while (s[start] != '\0' && index < len)
			newstr[index++] = s[start++];
	}
	newstr[index] = '\0';
	return (newstr);
}
